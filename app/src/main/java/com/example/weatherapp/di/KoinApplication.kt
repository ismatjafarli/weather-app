package com.example.weatherapp.di

import android.app.Application
import com.example.weatherapp.data.mappers.weatherMapperModule
import com.example.weatherapp.data.repositories.firebaseRepository
import com.example.weatherapp.data.repositories.weatherRepositoryImplModule
import com.example.weatherapp.domain.usecases.getMessagesUseCase
import com.example.weatherapp.domain.usecases.getWeatherUseCase
import com.example.weatherapp.domain.usecases.sendMessagesUseCase
import com.example.weatherapp.presentation.viewmodels.firebaseViewModel
import com.example.weatherapp.presentation.viewmodels.weatherViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class KoinApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            // Log Koin into Android logger
            androidLogger()
            // Reference Android context
            androidContext(this@KoinApplication)
            // Load modules
            modules(
                listOf(
                    appModule,
                    weatherRepositoryImplModule,
                    weatherViewModelModule,
                    weatherMapperModule,
                    getWeatherUseCase,
                    firebaseRepository,
                    getMessagesUseCase,
                    sendMessagesUseCase,
                    firebaseViewModel
                )
            )
        }

    }

}