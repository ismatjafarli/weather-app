package com.example.weatherapp.di

import com.example.weatherapp.data.network.WeatherApi
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {
    single { provideDatabaseReferenceIsmat() }
    factory { provideWeatherApi(get()) }
    single { provideRetrofit() }
}

private fun provideDatabaseReferenceIsmat(): DatabaseReference {
    return FirebaseDatabase.getInstance().reference
}

private fun provideWeatherApi(retrofit: Retrofit): WeatherApi {
    return retrofit.create(WeatherApi::class.java)
}

private fun provideRetrofit(): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://api.openweathermap.org/data/2.5/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}





