package com.example.weatherapp.data.mappers

import com.example.weatherapp.data.models.Main
import com.example.weatherapp.data.models.Weather
import com.example.weatherapp.data.models.WeatherData
import org.koin.dsl.module

val weatherMapperModule = module {
    single { WeatherMapper() }
}

class WeatherMapper {

    fun toWeatherDataDetails(weatherDataServer: WeatherData): com.example.weatherapp.domain.models.WeatherDataDetails {

        return com.example.weatherapp.domain.models.WeatherDataDetails(
            main = toMainDetails(weatherDataServer.main),
            name = weatherDataServer.name,
            weather = toWeatherDetails(weatherDataServer.weather[0])
        )
    }

    private fun toMainDetails(main: Main): com.example.weatherapp.domain.models.MainDetails {
        return com.example.weatherapp.domain.models.MainDetails(
            temp = main.temp,
            tempMax = main.tempMax,
            tempMin = main.tempMin
        )
    }

    private fun toWeatherDetails(weather: Weather): com.example.weatherapp.domain.models.WeatherDetails {
        return com.example.weatherapp.domain.models.WeatherDetails(
            main = weather.main
        )
    }

}