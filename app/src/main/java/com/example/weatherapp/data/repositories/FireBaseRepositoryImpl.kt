package com.example.weatherapp.data.repositories

import android.util.Log
import com.example.weatherapp.domain.models.Message
import com.example.weatherapp.domain.repositories.FireBaseRepository
import com.google.firebase.database.*
import org.koin.dsl.module

val firebaseRepository = module {
    single<FireBaseRepository> {
        FireBaseRepositoryImpl(
            get()
        )
    }
}

class FireBaseRepositoryImpl(
    private val databaseReference: DatabaseReference
) : FireBaseRepository {

    private val messageList = ArrayList<Message>()

    override fun getMessages(loadMessages: (List<Message>) -> Unit) {
        val queryI = databaseReference.child("Ismat")
        val queryS = databaseReference.child("Sexavet")
        if(messageList.isNotEmpty()) {
            messageList.clear()
        }

        queryI.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {

                    val message = snapshot.getValue(Message::class.java)
                    if (message != null) {
                        message.id = snapshot.key
                        messageList.add(message)
                    }
                loadMessages.invoke(messageList)
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                TODO("Not yet implemented")
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })

        queryS.addChildEventListener(object : ChildEventListener {

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val message = snapshot.getValue(Message::class.java)
                snapshot
                if (message != null) {
                    message.id = snapshot.key
                    messageList.add(message)
                }

                    loadMessages.invoke(messageList)
                    Log.e("AddM", messageList.toString())


            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                TODO("Not yet implemented")
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }


    override suspend fun sendMessages(message: Message) {
        databaseReference.child("Sexavet").push().setValue(message)
    }

}