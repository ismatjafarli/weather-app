package com.example.weatherapp.data.repositories

import com.example.weatherapp.data.mappers.WeatherMapper
import com.example.weatherapp.data.network.WeatherApi
import com.example.weatherapp.domain.models.WeatherDataDetails
import com.example.weatherapp.domain.repositories.WeatherRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.dsl.module

val weatherRepositoryImplModule = module {
    single<WeatherRepository> { WeatherRepositoryImpl(get(), get()) }
}

class WeatherRepositoryImpl(
    private val weatherApi: WeatherApi,
    private val weatherMapperr: WeatherMapper
) : WeatherRepository {

    override suspend fun getDailyForecast(parameters: Map<String, Double>): WeatherDataDetails {

        return withContext(Dispatchers.IO) {
            weatherMapperr.toWeatherDataDetails(weatherApi.getDailyForecast(parameters).body()!!)
        }

    }

}