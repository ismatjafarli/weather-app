package com.example.weatherapp.data.network

import com.example.weatherapp.data.models.WeatherData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface WeatherApi {

    companion object {
       const val WEATHER_API_KEY = "e5a8f1c7121a344e565898cea5be8ee5"
        const val UNITS = "metric"
    }

    @GET("weather/?appid=$WEATHER_API_KEY&units=$UNITS")
    suspend fun getDailyForecast(@QueryMap parameters: Map<String, Double>): Response<WeatherData>

}