package com.example.weatherapp.domain.models

data class WeatherDetails(
    val main: String
)
