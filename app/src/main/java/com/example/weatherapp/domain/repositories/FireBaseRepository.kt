package com.example.weatherapp.domain.repositories

import com.example.weatherapp.domain.models.Message

interface FireBaseRepository {

    fun getMessages(loadMessages: (List<Message>) -> Unit)

    suspend fun sendMessages(message: Message)

}