package com.example.weatherapp.domain.usecases

import com.example.weatherapp.domain.models.WeatherDataDetails
import com.example.weatherapp.domain.repositories.WeatherRepository
import org.koin.dsl.module

val getWeatherUseCase = module {
    factory { GetWeatherDataUseCase(get()) }

}

class GetWeatherDataUseCase(
    private val weatherRepository: WeatherRepository
) : ResponseUseCase<WeatherDataDetails> {

    override suspend fun execute(parameters: Map<String, Double>): WeatherDataDetails {
        return weatherRepository.getDailyForecast(parameters)
    }


}