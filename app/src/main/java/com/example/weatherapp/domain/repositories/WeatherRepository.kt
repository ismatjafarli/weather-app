package com.example.weatherapp.domain.repositories

import com.example.weatherapp.domain.models.WeatherDataDetails


interface WeatherRepository {

   suspend fun getDailyForecast(parameters: Map<String, Double>): WeatherDataDetails

}