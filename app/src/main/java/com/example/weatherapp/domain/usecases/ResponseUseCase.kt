package com.example.weatherapp.domain.usecases


interface ResponseUseCase<R> {

    suspend fun execute(parameters: Map<String, Double>): R

}