package com.example.weatherapp.domain.models

data class Message(
    var id: String? = "",
    var message: String? = "",
    var type: Int? = 0,
    var sendTime: Long? = 0
)
