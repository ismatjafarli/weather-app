package com.example.weatherapp.domain.usecases

import com.example.weatherapp.domain.models.Message
import com.example.weatherapp.domain.repositories.FireBaseRepository
import org.koin.dsl.module

val sendMessagesUseCase = module {
    factory { SendMessagesUseCase(get()) }
}

class SendMessagesUseCase(
    private val fireBaseRepository: FireBaseRepository
) {

    suspend fun execute(message: Message) {
        fireBaseRepository.sendMessages(message)
    }

}