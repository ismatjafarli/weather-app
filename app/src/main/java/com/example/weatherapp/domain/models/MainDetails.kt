package com.example.weatherapp.domain.models

data class MainDetails(
    val temp: Double,
    val tempMax: Double,
    val tempMin: Double
)
