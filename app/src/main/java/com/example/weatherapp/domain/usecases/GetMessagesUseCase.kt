package com.example.weatherapp.domain.usecases

import com.example.weatherapp.domain.models.Message
import com.example.weatherapp.domain.repositories.FireBaseRepository
import org.koin.dsl.module

val getMessagesUseCase = module {
    factory { GetMessagesUseCase(get()) }
}

class GetMessagesUseCase(
    private val fireBaseRepository: FireBaseRepository
) {
    fun execute(load: (List<Message>) -> Unit) {
        fireBaseRepository.getMessages {
            load.invoke(it)
        }
    }
}