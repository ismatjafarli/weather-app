package com.example.weatherapp.domain.models

data class WeatherDataDetails(
    val main: MainDetails,
    val name: String,
    val weather: WeatherDetails
)
