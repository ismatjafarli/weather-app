package com.example.weatherapp.presentation.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.domain.models.Message
import com.example.weatherapp.presentation.viewholders.ChatHolder
import com.example.weatherapp.presentation.viewholders.MyMessagesHolder

class ChatAdapter(
) : ListAdapter<Message, RecyclerView.ViewHolder>(MessagesDiffUtil()) {

    private lateinit var holder: RecyclerView.ViewHolder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            1 -> holder = ChatHolder(parent)
            0 -> holder = MyMessagesHolder(parent)
        }
        return holder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ChatHolder -> holder.bind(currentList[position])
            is MyMessagesHolder -> holder.bind(currentList[position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return currentList[position].type!!
    }

}