package com.example.weatherapp.presentation.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.*
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.weatherapp.R
import com.example.weatherapp.databinding.FragmentMapBinding
import com.example.weatherapp.data.models.Coordinate
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit


class MapFragment :
    BaseFragment<FragmentMapBinding>(FragmentMapBinding::inflate),
    OnMapReadyCallback {

    private lateinit var locationPermissionResult: ActivityResultLauncher<Array<String>>

    private lateinit var gpsEnabledResult: ActivityResultLauncher<Intent>

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var locationRequest: LocationRequest

    private lateinit var locationCallback: LocationCallback

    private val args: MapFragmentArgs by navArgs()

    private lateinit var navigateCoordinates: NavDirections

    private lateinit var autocompleteFragment: AutocompleteSupportFragment

    private lateinit var supportMapFragment: SupportMapFragment

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFusedLocationClient()
        setLocationRequest()
        setLocationCallback()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setSupportMapFragment()
        setAutocompleteSupportFragment()
        searchPlace()
        locationPermissionResult()
        gpsEnabledResult()
        setGetLocationButton()
    }

    private fun setFusedLocationClient() {
        fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(requireActivity())
    }

    private fun setLocationRequest() {
        Log.e("setLocationRequest()", "CALLED")
        locationRequest = LocationRequest.Builder(
            Priority.PRIORITY_HIGH_ACCURACY, TimeUnit.SECONDS.toMillis(1000L)
        ).apply {
            setMinUpdateDistanceMeters(1000F)
            setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)
            setWaitForAccurateLocation(true)
        }.build()
    }

    private fun setLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                Log.e("setLocationCallback()", locationResult.lastLocation!!.latitude.toString())
                locationResult.lastLocation?.let {
                    moveCameraAnimate(it.latitude, it.longitude)
                }
            }
        }
    }

    private fun setGetLocationButton() {
        binding.imageButton.setOnClickListener {
            getLocation()
        }
    }

    private fun gpsEnabledResult() {
        gpsEnabledResult =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (isLocationEnabled()) {
                    getLocation()
                }
            }
    }

    private fun locationPermissionResult() {
        locationPermissionResult = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { permissions ->
            when {
                permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                    getLocation()
                }
                permissions.getOrDefault(Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {

                }
                else -> {
                    Toast.makeText(
                        requireContext(),
                        "You will not be able to get current location unless you allowed location permission!",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }
        }
    }

    private fun setAutocompleteSupportFragment() {
        Places.initialize(requireContext(), "AIzaSyCqxAdRQRJyk1LjNgyOJXJUNY1H09TzB9E")
        autocompleteFragment =
            childFragmentManager.findFragmentById(R.id.searchView) as AutocompleteSupportFragment
    }

    private fun setSupportMapFragment() {

        supportMapFragment =
            childFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        supportMapFragment.getMapAsync(this)
    }

    private fun searchPlace() {

        autocompleteFragment.setPlaceFields(
            listOf(
                Place.Field.LAT_LNG,
            )
        )

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                sendLocation(place.latLng!!.latitude, place.latLng!!.longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.latLng!!, 14f));

            }

            override fun onError(status: Status) {
                Log.i("TAG", "An error occurred: $status")
            }
        })

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true
        setMarker()
        moveCamera()
    }


    private fun setMarker() {
        if (args.coordinate != null) {
            moveCameraAnimate(args.coordinate!!.latitude, args.coordinate!!.longitude)
        } else if (args.lat != 0.0f && args.lon != 0.0f) {
            moveCameraAnimate(args.lat.toDouble(), args.lon.toDouble())
        } else {
            val lastPlace = LatLng(40.4093, 49.8671)
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastPlace, 14f))
        }

    }

    private fun moveCameraAnimate(lat: Double, lon: Double) {
        setLocationSearchView(lat, lon)
        val lastPlace = LatLng(lat, lon)
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastPlace, 14f))
    }

    private fun sendLocation(lat: Double, lon: Double) {
        binding.buttonShowWeatherInfo.setOnClickListener {
            val coordinate = Coordinate(lat, lon)
            navigateCoordinates =
                MapFragmentDirections.actionMapFragmentToInformationFragment(coordinate)
            findNavController().navigate(navigateCoordinates)
        }
    }


    private fun setLocationSearchView(lat: Double, lon: Double) {

        lifecycleScope.launch {
            val geocoder = Geocoder(activity, Locale.getDefault())
            val addresses = withContext(Dispatchers.Default) {
                try {
                    geocoder.getFromLocation(lat, lon, 1)
                } catch (e: IOException) {
                    Log.e("IOException", e.toString())
                    null
                } catch (e: IllegalArgumentException) {
                    Log.e("IllegalArgumentException", e.toString())
                    null
                }
            }
            if (addresses != null && addresses.isNotEmpty()) {
                val address = addresses[0].getAddressLine(0)
                autocompleteFragment.setText(address.toString())
            } else {
                autocompleteFragment.setText("No Information!")
            }
        }
    }

    private fun moveCamera() {
        mMap.setOnCameraIdleListener {
            val center = mMap.cameraPosition.target
            setLocationSearchView(center.latitude, center.longitude)
            sendLocation(center.latitude, center.longitude)
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        return (ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
                )
    }

    private fun requestPermissions() {
        locationPermissionResult.launch(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )

    }

    private fun showAlertDialog() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Access location")
        builder.setMessage("Please, turn on device location to get current location!")
        builder.setPositiveButton("Yes") { _, _ ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            gpsEnabledResult.launch(intent)
        }
        builder.setNegativeButton("No thanks") { _, _ ->

        }
        builder.create().show()
    }

    @SuppressLint("MissingPermission", "SetTextI18n")
    private fun getLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                setLocationRequest()
                setLocationCallback()
                Log.e("getLocation", "invoked")
                fusedLocationClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.getMainLooper()
                )

            } else {
                showAlertDialog()
            }
        } else {
            requestPermissions()
        }
    }

}







