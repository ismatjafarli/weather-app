package com.example.weatherapp.presentation.adapters

import androidx.recyclerview.widget.DiffUtil
import com.example.weatherapp.domain.models.Message

class MessagesDiffUtil : DiffUtil.ItemCallback<Message>() {

    override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean {
        return oldItem == newItem
    }

}