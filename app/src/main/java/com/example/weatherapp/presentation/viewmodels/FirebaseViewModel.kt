package com.example.weatherapp.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weatherapp.domain.models.Message
import com.example.weatherapp.domain.usecases.GetMessagesUseCase
import com.example.weatherapp.domain.usecases.SendMessagesUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val firebaseViewModel = module {
    viewModel {
        FirebaseViewModel(
            get(),
            get()
        )
    }
}

class FirebaseViewModel(
    private val getMessagesUseCase: GetMessagesUseCase,
    private val sendMessagesUseCase: SendMessagesUseCase
) : ViewModel() {
    var messages = MutableLiveData<List<Message>>()

    fun getMessages() {
        getMessagesUseCase.execute {
            messages.value = it
        }
    }

    fun sendMessages(message: Message) {
        CoroutineScope(Dispatchers.Main).launch {
            sendMessagesUseCase.execute(message)
        }

    }

}