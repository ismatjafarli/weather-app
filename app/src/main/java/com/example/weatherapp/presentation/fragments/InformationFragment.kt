package com.example.weatherapp.presentation.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.weatherapp.R
import com.example.weatherapp.databinding.FragmentInformationBinding
import com.example.weatherapp.domain.models.WeatherDataDetails
import com.example.weatherapp.presentation.viewmodels.WeatherViewModel
import com.google.android.gms.location.*
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap

class InformationFragment :
    BaseFragment<FragmentInformationBinding>(FragmentInformationBinding::inflate) {

    private lateinit var locationPermissionResult: ActivityResultLauncher<Array<String>>

    private lateinit var gpsEnabledResult: ActivityResultLauncher<Intent>

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var locationRequest: LocationRequest

    private lateinit var locationCallback: LocationCallback

    private val weatherViewModel: WeatherViewModel by activityViewModel()

    private val args: InformationFragmentArgs by navArgs()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFusedLocationClient()
        setLocationRequest()
        setLocationCallback()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigateMapFragment()
        navigateChatFragment()
        copyUrl()
        locationPermissionResult()
        showForecast()
        gpsEnabledResult()
    }

    private fun setFusedLocationClient() {
        fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(requireActivity())
    }

    private fun setLocationRequest() {
        locationRequest = LocationRequest.Builder(
            Priority.PRIORITY_HIGH_ACCURACY,
            TimeUnit.SECONDS.toMillis(1000L)
        ).apply {
            setMinUpdateDistanceMeters(1000F)
            setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)
            setWaitForAccurateLocation(true)
        }.build()
    }

    private fun setLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                locationResult.lastLocation?.let {
                    Log.e("Location", it.toString())
                    val latitude = it.latitude
                    val longitude = it.longitude
                    getForecast(latitude, longitude)
                    navigateMapFragment(latitude, longitude)
                    copyUrl(latitude, longitude)
                }
            }
        }
    }

    private fun gpsEnabledResult() {
        gpsEnabledResult =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (isLocationEnabled()) {
                    getLocation()
                }
            }
    }

    private fun copyUrl(lat: Double = 0.0, lon: Double = 0.0) {
        var latitude: Double
        var longitude: Double
        binding.imageViewCopyText.setOnClickListener {
            if (args.coordinate != null) {
                latitude = args.coordinate!!.latitude
                longitude = args.coordinate!!.longitude
            } else {
                latitude = lat
                longitude = lon
            }
            val textToCopy =
                "https://api.openweathermap.org/data/2.5/weather?lat=$latitude&lon=$longitude"
            val clipboardManager =
                requireActivity().getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText("text", textToCopy)
            clipboardManager.setPrimaryClip(clipData)
            Toast.makeText(context, "Text copied to clipboard", Toast.LENGTH_LONG).show()
        }
    }

    private fun navigateChatFragment() {
        binding.fab.setOnClickListener {
            findNavController().navigate(R.id.action_informationFragment_to_chatFragment)
        }
    }

    private fun navigateMapFragment(lat: Double = 0.0, lon: Double = 0.0) {
        if (args.coordinate != null) {
            binding.buttonSelectPlace.setOnClickListener {
                val navigation =
                    InformationFragmentDirections.actionInformationFragmentToMapFragment(args.coordinate)
                findNavController().navigate(navigation)
            }
        } else {
            binding.buttonSelectPlace.setOnClickListener {
                val navigation =
                    InformationFragmentDirections.actionInformationFragmentToMapFragment(
                        lat = lat.toFloat(),
                        lon = lon.toFloat()
                    )
                findNavController().navigate(navigation)
            }
        }

    }

    private fun showForecast() {
        if (args.coordinate != null) {
            Log.e("showForecast()", "called")
            setProgressBarVisible()
            val latitude = args.coordinate!!.latitude
            val longitude = args.coordinate!!.longitude
            getForecast(latitude, longitude)
        } else {
            getLocation()
        }
    }

    private fun setWeatherImage(weatherDataDetails: WeatherDataDetails) {
        val main = weatherDataDetails.weather.main

        binding.apply {
            when (main) {
                "Mist" -> imageView.setImageResource(R.drawable.mist)
                "Clear" -> imageView.setImageResource(R.drawable.sunny)
                "Rain" -> imageView.setImageResource(R.drawable.rainy)
                "Clouds" -> imageView.setImageResource(R.drawable.clody)
                "Snow" -> imageView.setImageResource(R.drawable.snow)
            }
        }

    }

    private fun setDetails(weatherDataDetails: WeatherDataDetails) {
        binding.textViewPlace.text = weatherDataDetails.name
        binding.textViewDescription.text = weatherDataDetails.weather.main
        binding.textViewTemperature.text = weatherDataDetails.main.temp.toString()
        binding.textViewTempMax.text = weatherDataDetails.main.tempMax.toString()
        binding.textViewTempLow.text = weatherDataDetails.main.tempMin.toString()
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            requireContext().getSystemService(LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        return (ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermissions() {
        locationPermissionResult.launch(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )

    }

    private fun locationPermissionResult() {
        locationPermissionResult = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { permissions ->
            when {
                permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                    getLocation()
                }
                permissions.getOrDefault(Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {

                }
                else -> {
                    Toast.makeText(
                        requireContext(),
                        "You will not be able to get the current location weather info unless you allowed location permission!",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }
        }
    }

    @SuppressLint("MissingPermission", "SetTextI18n")
    private fun getLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                setProgressBarVisible()
                fusedLocationClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.getMainLooper()
                )

            } else {
                showAlertDialog()
            }
        } else {
            requestPermissions()
        }
    }

    private fun showAlertDialog() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Access location")
        builder.setMessage("Please, turn on device location to get the current location!")
        builder.setPositiveButton("Yes") { _, _ ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            gpsEnabledResult.launch(intent)
        }
        builder.setNegativeButton("No thanks") { _, _ ->

        }
        builder.create().show()
    }

    private fun setProgressBarVisible() {
        binding.progressBar.visibility = View.VISIBLE
        binding.groupMain.visibility = View.INVISIBLE
    }

    private fun getForecast(latitude: Double, longitude: Double) {
        val queries = HashMap<String, Double>()
        queries["lat"] = latitude
        queries["lon"] = longitude
        weatherViewModel.getDailyForecast(queries)
        weatherViewModel.dailyForeCast.observe(viewLifecycleOwner) { weatherDataDetails ->
            Log.e("checkObserver", "dd")

            binding.progressBar.visibility = View.INVISIBLE
            binding.groupMain.visibility = View.VISIBLE

            setDetails(weatherDataDetails)
            setWeatherImage(weatherDataDetails)
        }
    }



    override fun onDestroyView() {
        super.onDestroyView()
        Log.e("onDestroyView", "Got destroyed!")
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }


}