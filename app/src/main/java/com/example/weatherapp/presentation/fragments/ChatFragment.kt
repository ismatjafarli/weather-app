package com.example.weatherapp.presentation.fragments

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weatherapp.databinding.FragmentChatBinding
import com.example.weatherapp.presentation.adapters.ChatAdapter
import com.example.weatherapp.presentation.viewmodels.FirebaseViewModel
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import java.util.*

class ChatFragment : BaseFragment<FragmentChatBinding>(FragmentChatBinding::inflate) {

    private lateinit var chatAdapter: ChatAdapter

    private val databaseReference: DatabaseReference by inject()

    private val firebaseViewModel: FirebaseViewModel by activityViewModel()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.group2.visibility = View.INVISIBLE
        setAdapter()
        sendMessages()
        getMessages()
    }


    private fun sendMessages() {
        binding.buttonSendMessage.setOnClickListener {
            if (binding.editTextMessage.text.isNotEmpty()) {

                val current = Calendar.getInstance().timeInMillis
                val message = com.example.weatherapp.domain.models.Message("", binding.editTextMessage.text.toString(), 1, current)
                firebaseViewModel.sendMessages(message = message)
                binding.editTextMessage.text.clear()
            }

        }
    }

    private fun getMessages() {
        binding.buttonFindUser.setOnClickListener {
            val connectedRef = Firebase.database.getReference(".info/connected")
            databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
                @RequiresApi(Build.VERSION_CODES.O)
                override fun onDataChange(snapshot: DataSnapshot) {

                    if (snapshot.hasChild(binding.editTextUser.text.toString())) {
                        binding.group1.visibility = View.GONE
                        binding.group2.visibility = View.VISIBLE
                        firebaseViewModel.getMessages()

                        firebaseViewModel.messages.observe(viewLifecycleOwner) { list ->

                            chatAdapter.submitList(list.sortedBy { it.sendTime }.toMutableList())
                            if(list.isNotEmpty()) {
                                binding.recyclerView.smoothScrollToPosition(list.size - 1)
                            }

                        }

                    } else {
                        Toast.makeText(context, "User not found", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onCancelled(error: DatabaseError) {

                }

            })

        }
    }

    private fun setAdapter() {
        chatAdapter = ChatAdapter()
        binding.recyclerView.adapter = chatAdapter
        binding.recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("Destroyed", "D")
    }


}