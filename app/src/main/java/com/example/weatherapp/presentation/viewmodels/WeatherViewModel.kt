package com.example.weatherapp.presentation.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weatherapp.data.models.WeatherData
import com.example.weatherapp.domain.models.WeatherDataDetails
import com.example.weatherapp.domain.repositories.WeatherRepository
import com.example.weatherapp.domain.usecases.GetWeatherDataUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val weatherViewModelModule =  module {
    viewModel { WeatherViewModel(get())}
}

class WeatherViewModel(
    private val weatherDataUseCase: GetWeatherDataUseCase
): ViewModel() {
    var dailyForeCast = CreateLiveData.createLiveData()

    fun getDailyForecast(parameters: Map<String, Double>) {
        viewModelScope.launch{
            dailyForeCast.value = weatherDataUseCase.execute(parameters)
        }
    }

    override fun onCleared() {
        super.onCleared()
        Log.e("onCleared", "Cleared")
    }
}