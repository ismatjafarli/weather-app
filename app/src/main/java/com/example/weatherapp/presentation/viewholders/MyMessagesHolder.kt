package com.example.weatherapp.presentation.viewholders

import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.databinding.MessageItemBinding
import com.example.weatherapp.databinding.MessageItemMeBinding
import com.example.weatherapp.domain.models.Message

class MyMessagesHolder(
    private val parent: ViewGroup,
    private val binding: MessageItemMeBinding = MessageItemMeBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    )
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(message: Message) {
        binding.textView.text = message.message
    }

}

