package com.example.weatherapp.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import com.example.weatherapp.domain.models.WeatherDataDetails

class CreateLiveData {

    companion object {
        fun createLiveData(): MutableLiveData<WeatherDataDetails> {
            return MutableLiveData<WeatherDataDetails>()
        }
    }

}